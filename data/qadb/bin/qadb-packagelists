#!/usr/bin/python

# Copyright (c) 2009, 2010 Christoph Berg <myon@debian.org>
#
# getdpkginfo by
# Copyright (C) 2005  Jeroen van Wolffelaar <jeroen@wolffelaar.nl>
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The name of the author may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import apt_pkg, psycopg2, os, re, sys, time

ftp_prefix = "/srv/qa.debian.org/ftp"

def getdpkginfo(archive, deb):
    debpath = "%s/%s/%s" % (ftp_prefix, archive, deb)
    if verbose:
        print "  Reading", debpath
    dpkginfo = os.popen("dpkg-deb -I %s control" % debpath)
    section = priority = None
    for dpkgline in dpkginfo:
        if dpkgline.find(':') == -1: continue
        (key, value) = dpkgline.split(':', 1)
        key = key.lower()
        if key == "section":
            section = value.strip()
        elif key == "priority":
            priority = value.strip()
    if dpkginfo.close():
        #raise Exception, "Failed to read %s" % deb
        print "Failed to read %s" % deb
    return (section, priority)

pg = psycopg2.connect('service=qa')
cur = pg.cursor()
cur.execute("SET search_path TO apt")

def parseFile(suite_id, archive, suite, component, architecture, packagesfile):
    # this really needs to be rewritten to use some python lib instead
    if not os.path.isfile(packagesfile):
        raise Exception("%s not found" % packagesfile)
    if packagesfile.endswith(".xz"):
        stdout = os.popen("xzcat '%s'" % packagesfile)
    elif packagesfile.endswith(".gz"):
        stdout = os.popen("zcat '%s'" % packagesfile)
    else:
        raise Exception("Package file extension not recognized: '%s'" % packagefile)
    parse = apt_pkg.TagFile(stdout)

    source_re = re.compile('(.*) \((.*)\)')
    binnmu_re = re.compile('\+b\d+$')
    # There must be a smarter way than to require the last char to be a >
    uploaders_re = re.compile('[^,@ ][^@]+@[^@]+>')
    striplong_re = re.compile('\n.*')

    # clear the suite
    cur.execute("BEGIN")
    cur.execute("""DELETE FROM packagelist WHERE suite_id = %s""", [suite_id])

    # for every package ...
    while parse.step():
        package = parse.section.get('Package')
        version = parse.section.get('Version')
        control = "\n".join([ "%s: %s" % (k, parse.section.get(k)) \
                for k in parse.section.keys() ])

        if architecture == 'source':
            pkg_architecture = 'source'
            maintainer = parse.section.get('Maintainer')
            uploaders = parse.section.get('Uploaders')
            section = parse.section.get('Section')
            priority = parse.section.get('Priority')
            dm_upload_allowed = parse.section.get('Dm-Upload-Allowed') == 'yes'
        else:
            pkg_architecture = parse.section.get('Architecture')

        # store package control file
        cur.execute("""SELECT package_id FROM package
                       WHERE (package, version, pkg_architecture) = (%s, %s, %s)""",
                       [package, version, pkg_architecture])
        found = cur.fetchone()
        if found:
            package_id = found[0]
        else: # this is the first time we see this package, extract more information

            # There's no INSERT RETURNING in PG 8.1
            cur.execute("""INSERT INTO package (package, version, pkg_architecture)
                           VALUES (%s, %s, %s)""",
                           [package, version, pkg_architecture])
            cur.execute("""SELECT package_id FROM package
                           WHERE (package, version, pkg_architecture) = (%s, %s, %s)""",
                           [package, version, pkg_architecture])
            package_id = cur.fetchone()[0]
            cur.execute("""INSERT INTO package_control (package_id, control)
                           VALUES (%s, %s)""", [package_id, control])

            # this is a binary package
            if pkg_architecture <> 'source':
                # decode 'Source' field
                sourcefield = parse.section.get('Source')
                if sourcefield:
                    match = source_re.match(sourcefield)
                    if match:
                        source, source_version = match.group(1), match.group(2)
                    else:
                        source, source_version = sourcefield, version
                else:
                    source, source_version = package, version
                if binnmu_re.search(source_version):
                    print "WARNING: %s %s %s %s: source %s %s for %s %s (%s) has binnmu version number, fixing" % \
                        (archive, suite, component, architecture, source, source_version,
                                package, version, pkg_architecture)
                    source_version = binnmu_re.sub('', source_version)

                # store link to source package
                cur.execute("""SELECT package_id FROM package
                               WHERE (package, version, pkg_architecture) =
                                     (%s, %s, 'source')""", [source, source_version])
                found = cur.fetchone()
                if found:
                    cur.execute("""INSERT INTO package_source
                                   (package_id, source_id) VALUES (%s, %s)""",
                                   [package_id, found[0]])
                else:
                    print "WARNING: %s %s %s %s: source %s %s for %s %s (%s) not found" % \
                        (archive, suite, component, architecture, source, source_version,
                                package, version, pkg_architecture)

                # extract more fields
                description = re.sub(striplong_re, '', parse.section.get('Description'))
                if description:
                    cur.execute("""INSERT INTO package_info
                                   (package_id, field, value)
                                   VALUES (%s, 'Description', %s)""",
                                   [package_id, description])

                section = parse.section.get('Section')
                if section:
                    cur.execute("""INSERT INTO package_info
                                   (package_id, field, value)
                                   VALUES (%s, 'Section', %s)""",
                                   [package_id, section])

                priority = parse.section.get('Priority')
                if priority:
                    cur.execute("""INSERT INTO package_info
                                   (package_id, field, value)
                                   VALUES (%s, 'Priority', %s)""",
                                   [package_id, priority])

                filename = parse.section.get('Filename')
                if filename:
                    (debsection, debpriority) = getdpkginfo(archive, filename)
                    if debsection:
                        cur.execute("""INSERT INTO package_info
                                       (package_id, field, value)
                                       VALUES (%s, 'deb-Section', %s)""",
                                       [package_id, debsection])
                    if debpriority:
                        cur.execute("""INSERT INTO package_info
                                       (package_id, field, value)
                                       VALUES (%s, 'deb-Priority', %s)""",
                                       [package_id, debpriority])


            # this is a source package
            else:
                cur.execute("""INSERT INTO source
                               (package_id, maintainer, section, priority, dm_upload_allowed)
                               VALUES (%s, maint_id_or_new(%s), %s, %s, %s)""",
                               [package_id, maintainer, section, priority, dm_upload_allowed])

                if uploaders:
                    dudes = {}
                    for uploader in uploaders_re.findall(uploaders):
                        if dudes.has_key(uploader):
                            print "WARNING: %s %s %s %s: package %s has listed uploader %s twice" % \
                                    (archive, suite, component, architecture, package, uploader)
                            continue
                        cur.execute("""INSERT INTO uploader (package_id, maintainer)
                                       VALUES (%s, maint_id_or_new(%s))""",
                                       [package_id, uploader])
                        dudes[uploader] = 1

        # finally, add the package to the suite's package list
        cur.execute("""INSERT INTO packagelist
                       (suite_id, package_id) VALUES (%s, %s)""",
                       [suite_id, package_id])

    cur.execute("COMMIT")

verbose = sys.argv.__len__() > 1

cur.execute("""SELECT suite_id, archive, suite, component, architecture,
                  extract (epoch from last_update) AS last_update
               FROM suite
               WHERE active
               ORDER BY CASE WHEN architecture = 'source' THEN 1 ELSE 2 END
               FOR UPDATE NOWAIT""")
suites = cur.fetchall()

for suite_id, archive, suite, component, architecture, last_update in suites:
    if architecture == 'source':
        arch = 'source/Sources'
    else:
        arch = "binary-%s/Packages" % architecture
    packagesfile = ftp_prefix + "/%s/dists/%s/%s/%s" % \
            (archive, suite, component, arch)
    compression_ext = "xz"
    if not os.path.exists("%s.%s" % (packagesfile, compression_ext)):
        compression_ext = "gz"
        if not os.path.exists("%s.%s" % (packagesfile, compression_ext)):
            print "WARNING: %s.{xz,gz} not found" % packagesfile
            continue

    packagesfile = "%s.%s" % (packagesfile, compression_ext)
    mtime = os.path.getmtime(packagesfile)
    if not last_update or mtime > last_update:
        if verbose:
            print "Reading %s" % packagesfile
        parseFile(suite_id, archive, suite, component, architecture, packagesfile)

        update = """UPDATE suite SET last_update = %s WHERE suite_id = %s"""
        cur.execute(update, [time.ctime(mtime), suite_id])
        cur.execute("COMMIT")
    else:
        if verbose:
            print "Skipping up to date %s" % packagesfile

# Insert missing source packages:
# INSERT INTO package_source SELECT p.package_id, s.package_id as source_id from package p JOIN package s ON (p.package = s.package AND p.version = s.version AND s.pkg_architecture='source') where p.package_id not in (select package_id from package_source ) and p.pkg_architecture <> 'source';
# Insert missing source packages, packages with broken/missing binnmu Source: fieds:
# INSERT INTO package_source SELECT p.package_id, s.package_id as source_id from package p JOIN package s ON (p.package = s.package AND regexp_replace (p.version, E'\\+b\\d+$', '') = s.version AND s.pkg_architecture='source') where p.package_id not in (select package_id from package_source ) and p.pkg_architecture <> 'source';

