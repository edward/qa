#use wml::templ::template title="How to join" author="Raphaël Hertzog and Josip Rodin"

<H2>What needs to be done?</H2>

<P>Here's a short list of things that are in our scope:
<UL>
  <LI>When you find bugs - report them instantly! File 
      <A HREF="https://www.chiark.greenend.org.uk/~sgtatham/bugs.html">
      good bug reports</A>.
  <LI>Check if old bugs still exist, and work on them if they do.
  <LI>Correct buggy packages (by providing patches and suggestions).
  <LI>Provide more documentation where it's lacking.
  <LI>Check that all packages are policy conforming in general - Lintian
      <A HREF="https://lintian.debian.org/">reports</A> might point you
      to the right problems.
  <LI>Check that all packages are well integrated (menu, doc-base,
      alternatives, diversions, debconf, dhelp, dwww, info manuals,
      manual pages, etc.).
  <LI>Check dependencies, recommends, suggests, and all sorts of
      inter-package dependencies through configuration files or similar.
  <LI>Detect neglected, orphaned, and/or not used packages. Detect
      new upstream versions, too.
  <LI><a href="howto-remove.html">Remove orphaned packages that nobody wants
  or nobody uses.</a>
  <LI>Make sure that release critical bugs reports are handled and
      corrected rapidly.
  <LI>Simplify installation and upgrade of packages as well as the
      distribution (hint: debconf).
  <LI>Better interoperations between packages.
  <LI>Reviewal of the web presentation of Debian.
  <LI>All public relations of Debian (press releases, Debian Weekly News,
      other news, security announces).
</UL>

<FONT SIZE="-1">(originally written by Joey Schulze and others)</FONT>

<p>If you want more information about one of these tasks, feel free to ask
on the mailing list: <a href="mailto:debian-qa@lists.debian.org">debian-qa@lists.debian.org</a>.

